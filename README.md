# Tomáš Hübelbauer

- Check out Razor class libraries

## Projects

In broad strokes, these are the next steps for the things I am working on recently.

### Rust

**[Rust 1.26](Bookmark the 1.26 release announcement)**

### [Hubelbauer.net](http://hubelbauer.net)

Buy two more servers, use the existing one as a load balancer with Round Robin and balance between the two other servers.
Maybe also have a dedicated database server? And also, Deis will have to be used as opposed to Dokku (single-host).
https://deis.com/docs/workflow/quickstart/

- [ ] Reimage the server and reinstall all the applications
- [ ] Experiment with setting up LE using a wildcard or see if I can make it work with default app + apps on subdomains properly
- [ ] Display seach field in post pages as well
- [ ] Drop the header title "Tomas Hubelbauer" completely

### [QR Channel](https://gitlab.com/TomasHubelbauer/qr-channel)

- [ ] Wire up a demo which uses the combinatorically enumerated flow paths to establish a WebRTC data channel connection

### [Bloggo](http://hubelbauer.net)

- [ ] Finalize refactoring and iron out any issues

### [Editor](https://gitlab.com/TomasHubelbauer/editor)

### [Agenda](https://agenda.hubelbauer.net/)

[Contributing](https://gitlab.com/TomasHubelbauer/agenda/blob/master/README.md#contributing)

### [Tododo](https://tododo.hubelbauer.net/)

Remove from Dokku.

### Puppeteer-Based

- [ISDS tasks](https://gitlab.com/TomasHubelbauer/isds-puppeteer/tree/master/todo)

### MarkDown DOM

- Continue improving to be able to perfectly reconstruct the entirety of NPM readmes
- Develop a general *Format Document* extension built on MarkDownDOM
- Use in my VS Code extensions for obtaining header / checkbox item stripped text content
- Use in Bloggo to render article HTML and support additional functionality (todos, embeds)

### VS Code extension publisher install and rating watch

A simple Node app deployed to Dokku which collects installs and stars and puts them to Postgres in a specific interval.

The frontend displays a graph in time to reveal the stars rising/falling.

- [ ] Redeploy after fixing the server

### [My VS Code Extensions](https://gitlab.com/TomasHubelbauer/bloggo-vscode#my-extensions)

- [ ] Consider developing a video previewing extension after https://stackoverflow.com/q/50803834/2715716
- [ ] Consider creating an extension for running API code dynamically for prototyping
    - [ ] Use https://code.visualstudio.com/updates/v1_25#_terminal-renderers for REPL 
- [ ] Consider creating SVG language server with suggestions for attribute names
- [ ] Automate screenshot taking using [my suggested API](https://github.com/Microsoft/vscode/issues/49959) or otherwise
    - See the solution using `window-screenshot` in `vscode-box-drawing` for local Windows runs
        - Consider using this to generate extension icon too by cropping the editor screenshot
    - https://github.com/electron/electron/issues/13580 and the `bloggo-nodejs` repo
- [ ] Do opt-in telemetry with report review before sending using [`vscode-extension-telemetry`](https://github.com/Microsoft/vscode-extension-telemetry/issues/17)
- [ ] Create icons for all of the extensions (probably screenshots of visual details of the extensions in the UI)
- [ ] Harmonize the names and make the week one also MarkDown prefixed by contacting [viradhamMS or pkuma-msft](https://github.com/Microsoft/vscode/issues/48378)
- [ ] Create a meta extension with extension dependencies on all of these (*Tomas Hubelbauer's MarkDown Suite*)
- [ ] Make the Travis CI badges clickable
- [ ] Set up CI for all extensions and automate release cutting to keep release VSIXs in GitHub
    - [Inspire self by this](http://jasonpoon.ca/2016/11/15/continuous-delivery-of-visual-studio-code-extensions/)
    - [Use GitLab CI from GitHub with a runner on my own server if possible](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html)
        - Deploy tagged runs with the publisher key stored on my server

### GitPub

Work work work

Rename as this name has been taken by something which uses ActivityPub now.

### [Meter10](https://github.com/TomasHubelbauer/Meter10)

- [ ] Find out why the notification icon disappears on hover
- [ ] Draw the icons based on the values to a `Bitmap` and convert to `Icon`
- [ ] Figure out how to get the CPU and RAM values - WCP, P/Invoke?

Icon drafts: ![](cpu.png) & ![](ram.png) (but invert and make them fuzzy like the Windows ones)

### VS Code Extension Screenshots

- [Issue requesting API / extenshion host window support for this](https://github.com/Microsoft/vscode/issues/49959)

### RLS VS Code extension progress

https://github.com/rust-lang-nursery/rls-vscode/pull/306

Finish this when the VS Code API for making channels out of processes runs.

### VS Code CodeLens font configuration PR

https://github.com/Microsoft/vscode/pull/53990

Research further on how to read the composite font configuration information.

### IsoGit

#### Merge Conflict

https://github.com/isomorphic-git/isomorphic-git/issues/325

#### Performance Snapshotting

- [Context](https://github.com/isomorphic-git/isomorphic-git/pull/295#issuecomment-399780494) for this work
- [Jest is not interested in adding performance snapshotting](https://github.com/facebook/jest/issues/2694)
- [Karma has a framework for this](https://github.com/axemclion/karma-telemetry)
- [Benchmark.js also looks relevant](https://benchmarkjs.com/)
- [Karma Benchmark thingy](https://www.npmjs.com/package/karma-benchmark)
- [Jasmine load testing](https://github.com/robertgreiner/jasmine-load-test)
- [ ] Figure out IsoGit uses what and what could be introduced

### Sonic PI with VS Code

- https://github.com/samaaron/sonic-pi/issues/1902

### Luxon documentation links

- https://github.com/moment/luxon/issues/270

### Outdated TypeScript starter tutorial

- https://github.com/Microsoft/TypeScript-React-Starter/issues/163
- https://github.com/Microsoft/TypeScript-React-Starter/issues/168

### Hibernated

- [Pressure Draw](https://gitlab.com/TomasHubelbauer/pressure-draw)

## Domains

- [ ] Map redirects here
- [async-await.cz](https://async-await.cz)
- [async-await.net](https://async-await.net)
- [bloggo.net](https://bloggo.net)
- [gitlab-pages-custom-domain-lets-encrypt.info](https://gitlab-pages-custom-domain-lets-encrypt.info)
- [hubelbauer.cz](https://hubelbauer.cz)
- [hubelbauer.net](https://hubelbauer.net)
    - Bloggo
    - Agenda
    - Tododo
    - Vscode
- [katerinanikola.com](https://katerinanikola.com)
- [musicblackholes.com](https://musicblackholes.com)
- [rhaeo.com](https://rhaeo.com)
- [rhaeo.cz](https://rhaeo.cz)
- [rhaeo.net](https://rhaeo.net)
- [rhaeo.org](https://rhaeo.org)
- [tomashubelbauer.cz](https://tomashubelbauer.cz)
- [tomashubelbauer.net](https://tomashubelbauer.net)

## Profiles

- [Facebook: `tomas.hubelbauer`](https://www.facebook.com/tomas.hubelbauer)
- WhatsApp: `+420 607400***` (you can find this online if you really want)
- [Twitter: `TomasHubelbauer`](https://twitter.com/TomasHubelbauer)
- [Instagram: `tomashubelbauer`](https://www.instagram.com/tomashubelbauer)
- [YouTube (Google+): `+Tomáš Hübelbauer`](https://www.youtube.com/channel/UCr9Y1cBBWu5F4skyW2VS6pA)
- Skype: `tomashubelbauer`
- [LinkedIn: `tomashubelbauer`](https://www.linkedin.com/in/tomashubelbauer)
- [Meetup](https://www.meetup.com/members/242837929/)
- [Hacker News: `TomasHubelbauer`](https://news.ycombinator.com/user?id=TomasHubelbauer)
- [GitHub: `TomasHubelbauer`](https://github.com/TomasHubelbauer) + Rhaeo + Bloggo
- [GitLab: `TomasHubelbauer`](https://gitlab.com/TomasHubelbauer) + Rhaeo + Bloggo
